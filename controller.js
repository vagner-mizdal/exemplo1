exports.helloWorld = () => {
    return "Hello World!"
}

exports.soma = (a, b) => {
    return a + b
}

exports.multiplicacao = (a, b) => {
    return a * b
}
    
exports.divisao = (a, b) => {
    return a / b
}

exports.nome = (nome) => {
    return "Olá " + nome
}

exports.random = () => {
    return Math.random()
}